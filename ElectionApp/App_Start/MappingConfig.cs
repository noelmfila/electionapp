﻿using ElectionApp.Models;
using ElectionApp.ViewModels;
using AutoMapper;

namespace ElectionApp
{
    public static class MappingConfig
    {
        public static void RegisterMaps()
        {
           Mapper.Initialize(config =>
           {
               config.CreateMap<Voter, Voter>();
           });

        }


    }
}