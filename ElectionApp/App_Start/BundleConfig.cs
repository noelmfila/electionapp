﻿using System.Web.Optimization;

namespace ElectionApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Custom bundle
            bundles.Add(new ScriptBundle("~/bundles/electionApp").Include(
                      "~/Scripts/electionApp/RegistrationDate.js",
                      "~/Scripts/electionApp/ConfirmFoodRemoval.js",
                      "~/Scripts/electionApp/AddFavouriteFoods.js",
                      "~/Scripts/electionApp/DateOfBirth.js",
                      "~/Scripts/electionApp/ValidateDates.js",
                      "~/Scripts/electionApp/CalculateVoterAge.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/moment.js")); //for datetime picker

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            //JS
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                    "~/Scripts/jquery-ui-{version}.js"));
            //css  
            bundles.Add(new StyleBundle("~/Content/cssjqryUi").Include(
                   "~/Content/jquery-ui.css"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootbox.min.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-datetimepicker.js")); //for datetime picker

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css",
                      "~/Content/animate.css",
                      "~/Content/bootstrap-datetimepicker.css")); //for datetime picker
        }
    }
}
