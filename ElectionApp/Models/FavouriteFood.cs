﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectionApp.Models
{
    public class FavouriteFood
    {
        public int Id { get; set; }

        public string VoterUserId { get; set; }
        public Voter Voter { get; set; }

        public int FoodId { get; set; }
        public Food Food { get; set; }

      }
}