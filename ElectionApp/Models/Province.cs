﻿namespace ElectionApp.Models
{
    public class Province
    {
        public int Id { get; set; }
        public string ProvinceName { get; set; }
    }
}