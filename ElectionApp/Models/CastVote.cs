﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectionApp.Models
{
    public class CastVote
    {

        //Associate logged in user with Voter
        [Key]
        [Column(Order = 1)]
        public string VoterId { get; set; }
        public ApplicationUser Voter { get; private set; }

        //Party Info
        [Key]
        [Column(Order = 2)]
        public int PartyId { get; set; }
        public Party Party { get; private set; }

        public CastVote()
        {

        }

        public CastVote(Party party, ApplicationUser voter)
        {
            Party = party ?? throw new ArgumentNullException("party");

            if (voter == null)
                throw new ArgumentNullException("voter");
            Voter = voter;

        }

    }
}