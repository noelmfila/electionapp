﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectionApp.Models
{
    public class Voter
    {
        [Key, ForeignKey("ApplicationUser")]
        public string UserId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string Surname { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "ID Number")]
        public double IdNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Range(18, 150, ErrorMessage = "You must be between 18-150 years to vote!")]
        public int Age { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        public int ContactNumber { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Registration")]
        public DateTime RegistrationDate { get; set; }

        [Required]
        [Display(Name = "Race")]
        public int RaceId { get; set; }
        public Race Race { get; set; }

        [Required]
        [Display(Name = "Province")]
        public int ProvinceId { get; set; }
        public Province Province { get; set; }

        [Required]
        [Display(Name = "District")]
        public int DistrictId { get; set; }
        public District District { get; set; }

        [Required]
        [Display(Name = "Municipality")]
        public int MunicipalityId { get; set; }
        public Municipality Municipality { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

    }
}