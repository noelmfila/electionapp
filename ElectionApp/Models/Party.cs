﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ElectionApp.Models
{
    public class Party
    {
        public int Id { get; set; }

        [Required]
        [StringLength(90)]
        [Display(Name = "Party Name")]
        public string PartyName { get; set; }

        [Required]
        [StringLength(10)]
        public string Acronym { get; set; }

        [Required]
        [StringLength(100)]
        public string Representative { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Registration")]
        public DateTime RegistrationDate { get; set; }

    }
}