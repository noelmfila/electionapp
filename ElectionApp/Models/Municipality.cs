﻿namespace ElectionApp.Models
{
    public class Municipality
    {
        public int Id { get; set; }
        public string MunicipalityName { get; set; }
        public District District { get; set; }
        public int DistrictId { get; set; }
    }
}