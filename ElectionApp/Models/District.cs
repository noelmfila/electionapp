﻿namespace ElectionApp.Models
{
    public class District
    {
        public int Id { get; set; }
        public string DistrictName { get; set; }
        public Province Province { get; set; }
        public int ProvinceId { get; set; }
    }
}