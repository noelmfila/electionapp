﻿using System.ComponentModel.DataAnnotations;

namespace ElectionApp.Models
{
    public class Food
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Food Name")]
        public string FoodName { get; set; }
    }
}