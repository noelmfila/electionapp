﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace ElectionApp.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<Voter> Voters { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<Municipality> Municipalities { get; set; }
        public DbSet<Party> Parties { get; set; }
        public DbSet<CastVote> CastVotes { get; set; }
        public DbSet<Food> Foods { get; set; }
        public DbSet<FavouriteFood> FavouriteFoods { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Voter>().HasRequired(a => a.Province)
                .WithMany()
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Municipality>().HasRequired(b => b.District)
                .WithMany()
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<CastVote>().HasRequired(b => b.Party)
                .WithMany()
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<CastVote>().HasRequired(b => b.Voter)
                .WithMany()
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<FavouriteFood>().HasRequired(b => b.Voter)
                .WithMany()
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<FavouriteFood>().HasRequired(b => b.Food)
                .WithMany()
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}