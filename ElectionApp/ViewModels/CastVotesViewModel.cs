﻿using ElectionApp.Models;
using System.Collections.Generic;


namespace ElectionApp.ViewModels
{
    public class CastVotesViewModel
    {
        public Party Party { get; set; }

        public CastVote CastVote { get; set; }

        public int PartyId { get; set; }

        public IEnumerable<Party> Parties { get; set; }
        public string SearchTerm { get; set; }
    }
}