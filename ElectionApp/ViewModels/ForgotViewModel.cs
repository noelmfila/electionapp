﻿using System.ComponentModel.DataAnnotations;

namespace ElectionApp.ViewModels
{
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}