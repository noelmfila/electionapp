﻿using System.ComponentModel.DataAnnotations;

namespace ElectionApp.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}