﻿namespace ElectionApp.ViewModels
{
    public class ElectionResultsViewModel
    {
        public string PartyName { get; set; }
        public string Acronym { get; set; }
        public int Votes { get; set; }
    }
}