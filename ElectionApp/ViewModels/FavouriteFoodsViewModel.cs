﻿using ElectionApp.Models;
using System.Collections.Generic;

namespace ElectionApp.ViewModels
{
    public class FavouriteFoodsViewModel
    {
        public int? FavouriteId { get; set; }

        public int SelectedFoodId { get; set; }

        public Food Food { get; set; }

        public Voter Voter { get; set; }

        public IEnumerable<Food> Foods { get; set; }
    }
}