﻿using ElectionApp.Models;
using System.Collections.Generic;
using System.Web.Mvc;


namespace ElectionApp.ViewModels
{
    public class CreateVoterViewModel
    {
        public string Heading { get; set; }

        public IEnumerable<Race> Races { get; set; }

        public IEnumerable<SelectListItem> Provinces { get; set; }

        public IEnumerable<SelectListItem> Districts { get; set; }

        public IEnumerable<SelectListItem> Municipalities { get; set; }

        public Voter Voter { get; set; }

        public Race Race { get; set; }

    }
}