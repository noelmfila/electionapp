﻿using ElectionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ElectionApp.Repository
{
    public class DistrictsRepository
    {
        private readonly ApplicationDbContext _context;

        public DistrictsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetDistrictsByProvinceId()
        {
            var districts = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Value = (-1).ToString(),
                    Text = "Select District"
                }
            };
            return districts;
        }

        public IEnumerable<SelectListItem> GetDistrictsByProvinceId(string provinceId)
        {
            if (string.IsNullOrWhiteSpace(provinceId)) return null;
            using (_context)
            {
                IEnumerable<SelectListItem> districts = _context.Districts.AsNoTracking()
                    .OrderBy(d => d.DistrictName)
                    .Where(d => d.ProvinceId.ToString() == provinceId)
                    .Select(d =>
                        new SelectListItem
                        {
                            Value = d.Id.ToString(),
                            Text = d.DistrictName
                        }).ToList();
                return new SelectList(districts, "Value", "Text");
            }
        }
    }
}