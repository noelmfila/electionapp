﻿using ElectionApp.Models;
using ElectionApp.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace ElectionApp.Repository
{
    public class ResultsRepository
    {
        private readonly ApplicationDbContext _context;

        public ResultsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<ElectionResultsViewModel> GetResults()
        {
            var electionResults = _context.Parties
                    .Join(_context.CastVotes,
                        p => p.Id, c => c.PartyId,
                        (party, castVote) =>
                        new { party, castVote })
                    .GroupBy(x => new
                    { x.party.PartyName, x.party.Acronym })
                    .AsEnumerable()
                    .Select(g => new
                    ElectionResultsViewModel()
                    {
                        PartyName = g.Key.PartyName,
                        Acronym = g.Key.Acronym,
                        Votes = g.Count()
                    });
            return (electionResults);

        }
    }
}