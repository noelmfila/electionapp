﻿using ElectionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ElectionApp.Repository
{
    public class MunicipalitiesRepository
    {
        private readonly ApplicationDbContext _context;

        public MunicipalitiesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetMunicipalitiesByDistrictId()
        {
            var municipalities = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Value = (-1).ToString(),
                    Text = "Select Municipality"
                }
            };
            return municipalities;
        }

        public IEnumerable<SelectListItem> GetMunicipalitiesByDistrictId(string districtId)
        {
            if (string.IsNullOrWhiteSpace(districtId)) return null;
            using (_context)
            {
                IEnumerable<SelectListItem> municipalities = _context.Municipalities.AsNoTracking()
                    .OrderBy(m => m.MunicipalityName)
                    .Where(m => m.DistrictId.ToString() == districtId)
                    .Select(m =>
                        new SelectListItem
                        {
                            Value = m.Id.ToString(),
                            Text = m.MunicipalityName
                        }).ToList();
                return new SelectList(municipalities, "Value", "Text");
            }
        }
    }
}