﻿using ElectionApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ElectionApp.Repository
{
    public class ProvincesRepository
    {
        private readonly ApplicationDbContext _context;

        public ProvincesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetProvinces()
        {
            using (_context)
            {
                var provinces = _context.Provinces.AsNoTracking()
                                                 .OrderBy(p => p.ProvinceName)
                                                 .Select(p =>
                                                 new SelectListItem
                                                 {
                                                     Value = p.Id.ToString(),
                                                     Text = p.ProvinceName
                                                 }).ToList();
                var provincetip = new SelectListItem()
                {
                    Value = (-1).ToString(),
                    Text = "Select Province"
                };
                provinces.Insert(0, provincetip);
                return new SelectList(provinces, "Value", "Text");
            }

        }
    }
}