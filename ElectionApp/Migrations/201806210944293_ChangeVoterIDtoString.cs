namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeVoterIDtoString : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.CastVotes", new[] { "Voter_Id" });
            DropColumn("dbo.CastVotes", "VoterId");
            RenameColumn(table: "dbo.CastVotes", name: "Voter_Id", newName: "VoterId");
            AlterColumn("dbo.CastVotes", "VoterId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.CastVotes", "VoterId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CastVotes", new[] { "VoterId" });
            AlterColumn("dbo.CastVotes", "VoterId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.CastVotes", name: "VoterId", newName: "Voter_Id");
            AddColumn("dbo.CastVotes", "VoterId", c => c.Int(nullable: false));
            CreateIndex("dbo.CastVotes", "Voter_Id");
        }
    }
}
