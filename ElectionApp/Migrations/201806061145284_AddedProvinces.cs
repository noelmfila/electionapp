namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedProvinces : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProvinceName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Voters", "ProvinceId", c => c.Int(nullable: false));
            CreateIndex("dbo.Voters", "ProvinceId");
            AddForeignKey("dbo.Voters", "ProvinceId", "dbo.Provinces", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Voters", "ProvinceId", "dbo.Provinces");
            DropIndex("dbo.Voters", new[] { "ProvinceId" });
            DropColumn("dbo.Voters", "ProvinceId");
            DropTable("dbo.Provinces");
        }
    }
}
