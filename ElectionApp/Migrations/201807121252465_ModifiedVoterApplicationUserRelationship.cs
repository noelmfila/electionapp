namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedVoterApplicationUserRelationship : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Voters", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Voters", "UserId");
            AddForeignKey("dbo.Voters", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Voters", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Voters", new[] { "UserId" });
            DropColumn("dbo.Voters", "UserId");
        }
    }
}
