namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FurtherFavFoodsModification : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.FavouriteFoods");
            AddColumn("dbo.FavouriteFoods", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.FavouriteFoods", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.FavouriteFoods");
            DropColumn("dbo.FavouriteFoods", "Id");
            AddPrimaryKey("dbo.FavouriteFoods", new[] { "VoterUserId", "FoodId" });
        }
    }
}
