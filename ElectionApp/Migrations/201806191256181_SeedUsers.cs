namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'845fb7a3-4bb2-4862-957a-6437cfeb5767', N'admin@electionapp.com', 0, N'AMC6y1eLrlKUjLGmnezW+WWnwBWcUwbRKEttiGfKGyx20QJpfeWod6j7n1ao9O3ONQ==', N'3a188cc4-1945-4b1e-94e6-bec246cf2814', NULL, 0, 0, NULL, 1, 0, N'admin@electionapp.com')
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'9dbc4268-a051-4f15-8efd-b90df79a1713', N'guest@electionapp.com', 0, N'AMYHGtfLtQhEhelHSKz/6C9x4En3Db0v8KFKw13XjQLxyQ82dsDJesugSNUee9IkzQ==', N'0080ef29-92bf-403d-815f-3c478ba83d56', NULL, 0, 0, NULL, 1, 0, N'guest@electionapp.com')
            
            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'f2291ea4-6f7c-442d-9c18-64c907b239d5', N'CanManageParties')

            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'845fb7a3-4bb2-4862-957a-6437cfeb5767', N'f2291ea4-6f7c-442d-9c18-64c907b239d5')

                ");
        }
        
        public override void Down()
        {
        }
    }
}
