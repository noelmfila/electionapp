namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFavouriteFoodTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FavouriteFoods",
                c => new
                    {
                        VoterId = c.String(nullable: false, maxLength: 128),
                        FoodId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.VoterId, t.FoodId })
                .ForeignKey("dbo.Foods", t => t.FoodId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.VoterId, cascadeDelete: true)
                .Index(t => t.VoterId)
                .Index(t => t.FoodId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FavouriteFoods", "VoterId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FavouriteFoods", "FoodId", "dbo.Foods");
            DropIndex("dbo.FavouriteFoods", new[] { "FoodId" });
            DropIndex("dbo.FavouriteFoods", new[] { "VoterId" });
            DropTable("dbo.FavouriteFoods");
        }
    }
}
