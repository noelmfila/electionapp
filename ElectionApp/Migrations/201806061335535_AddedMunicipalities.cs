namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMunicipalities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Municipalities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MunicipalityName = c.String(),
                        DistrictId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Districts", t => t.DistrictId)
                .Index(t => t.DistrictId);
            
            AddColumn("dbo.Voters", "DistrictId", c => c.Int(nullable: false));
            AddColumn("dbo.Voters", "MunicipalityId", c => c.Int(nullable: false));
            CreateIndex("dbo.Voters", "DistrictId");
            CreateIndex("dbo.Voters", "MunicipalityId");
            AddForeignKey("dbo.Voters", "DistrictId", "dbo.Districts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Voters", "MunicipalityId", "dbo.Municipalities", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Voters", "MunicipalityId", "dbo.Municipalities");
            DropForeignKey("dbo.Voters", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.Municipalities", "DistrictId", "dbo.Districts");
            DropIndex("dbo.Voters", new[] { "MunicipalityId" });
            DropIndex("dbo.Voters", new[] { "DistrictId" });
            DropIndex("dbo.Municipalities", new[] { "DistrictId" });
            DropColumn("dbo.Voters", "MunicipalityId");
            DropColumn("dbo.Voters", "DistrictId");
            DropTable("dbo.Municipalities");
        }
    }
}
