namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedFavouriteFoodTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.FavouriteFoods", name: "VoterId", newName: "VoterUserId");
            RenameIndex(table: "dbo.FavouriteFoods", name: "IX_VoterId", newName: "IX_VoterUserId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.FavouriteFoods", name: "IX_VoterUserId", newName: "IX_VoterId");
            RenameColumn(table: "dbo.FavouriteFoods", name: "VoterUserId", newName: "VoterId");
        }
    }
}
