namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedVotersTable1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Voters", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Voters", "Id", c => c.Int(nullable: false));
        }
    }
}
