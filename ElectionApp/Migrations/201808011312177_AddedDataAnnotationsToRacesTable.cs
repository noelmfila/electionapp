namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDataAnnotationsToRacesTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Races", "RaceName", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Races", "RaceName", c => c.String());
        }
    }
}
