namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedFavouriteFoodsTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FavouriteFoods", "FoodId", "dbo.Foods");
            DropForeignKey("dbo.FavouriteFoods", "VoterUserId", "dbo.Voters");
            AddForeignKey("dbo.FavouriteFoods", "FoodId", "dbo.Foods", "Id");
            AddForeignKey("dbo.FavouriteFoods", "VoterUserId", "dbo.Voters", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FavouriteFoods", "VoterUserId", "dbo.Voters");
            DropForeignKey("dbo.FavouriteFoods", "FoodId", "dbo.Foods");
            AddForeignKey("dbo.FavouriteFoods", "VoterUserId", "dbo.Voters", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.FavouriteFoods", "FoodId", "dbo.Foods", "Id", cascadeDelete: true);
        }
    }
}
