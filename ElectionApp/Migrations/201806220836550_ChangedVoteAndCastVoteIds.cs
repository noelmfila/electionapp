namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedVoteAndCastVoteIds : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.CastVotes");
            AddPrimaryKey("dbo.CastVotes", new[] { "VoterId", "PartyId" });
            DropColumn("dbo.CastVotes", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CastVotes", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.CastVotes");
            AddPrimaryKey("dbo.CastVotes", "Id");
        }
    }
}
