namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedVotersTable : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Voters");
            AlterColumn("dbo.Voters", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Voters", "UserId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Voters");
            AlterColumn("dbo.Voters", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Voters", "Id");
        }
    }
}
