namespace ElectionApp.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateRaces : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Races (RaceName) VALUES ('African')");
            Sql("INSERT INTO Races (RaceName) VALUES ('Chinese')");
            Sql("INSERT INTO Races (RaceName) VALUES ('Coloured')");
            Sql("INSERT INTO Races (RaceName) VALUES ('Indian')");
            Sql("INSERT INTO Races (RaceName) VALUES ('White')");
        }

        public override void Down()
        {
        }
    }
}
