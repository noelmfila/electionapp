namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedVotersTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Voters", "IdNumber", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Voters", "IdNumber", c => c.Int(nullable: false));
        }
    }
}
