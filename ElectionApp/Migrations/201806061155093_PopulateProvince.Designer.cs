// <auto-generated />
namespace ElectionApp.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class PopulateProvince : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PopulateProvince));
        
        string IMigrationMetadata.Id
        {
            get { return "201806061155093_PopulateProvince"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
