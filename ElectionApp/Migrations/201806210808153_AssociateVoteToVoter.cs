namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssociateVoteToVoter : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CastVotes", "VoterId", "dbo.Voters");
            DropIndex("dbo.CastVotes", new[] { "VoterId" });
            AddColumn("dbo.CastVotes", "Voter_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.CastVotes", "Voter_Id");
            AddForeignKey("dbo.CastVotes", "Voter_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CastVotes", "Voter_Id", "dbo.AspNetUsers");
            DropIndex("dbo.CastVotes", new[] { "Voter_Id" });
            DropColumn("dbo.CastVotes", "Voter_Id");
            CreateIndex("dbo.CastVotes", "VoterId");
            AddForeignKey("dbo.CastVotes", "VoterId", "dbo.Voters", "Id");
        }
    }
}
