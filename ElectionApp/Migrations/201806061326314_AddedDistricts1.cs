namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDistricts1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Voters", "ProvinceId", "dbo.Provinces");
            AddForeignKey("dbo.Voters", "ProvinceId", "dbo.Provinces", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Voters", "ProvinceId", "dbo.Provinces");
            AddForeignKey("dbo.Voters", "ProvinceId", "dbo.Provinces", "Id", cascadeDelete: true);
        }
    }
}
