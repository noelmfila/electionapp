namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateProvince : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Eastern Cape')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Free State')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Gauteng')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Kwazulu-Natal')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Limpopo')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Mpumalanga')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Northern Cape')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('North West')");
            Sql("INSERT INTO Provinces(ProvinceName) VALUES ('Western Cape')");
        }
        
        public override void Down()
        {
        }
    }
}
