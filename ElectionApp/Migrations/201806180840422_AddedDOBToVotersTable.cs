namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDOBToVotersTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Voters", "dateOfBirth", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Voters", "dateOfBirth");
        }
    }
}
