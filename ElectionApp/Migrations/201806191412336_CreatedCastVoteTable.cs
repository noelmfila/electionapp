namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedCastVoteTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CastVotes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VoterId = c.Int(nullable: false),
                        PartyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parties", t => t.PartyId)
                .ForeignKey("dbo.Voters", t => t.VoterId)
                .Index(t => t.VoterId)
                .Index(t => t.PartyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CastVotes", "VoterId", "dbo.Voters");
            DropForeignKey("dbo.CastVotes", "PartyId", "dbo.Parties");
            DropIndex("dbo.CastVotes", new[] { "PartyId" });
            DropIndex("dbo.CastVotes", new[] { "VoterId" });
            DropTable("dbo.CastVotes");
        }
    }
}
