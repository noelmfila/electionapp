namespace ElectionApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateDistrict : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Alfred Nzo District Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Amajuba District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Amathole District Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Bojanala Platinum District Municipality', 8)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Buffalo City Metropolitan Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Cacadu District Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Cape Winelands District Municipality', 9)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Capricorn District Municipality', 5)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Central Karoo District Municipality', 9)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Chris Hani District Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('City of Cape Town Metropolitan Municipality', 9)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('City of Johannesburg Metropolitan Municipality', 3)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('City of Tshwane Metropolitan Municipality', 3)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Dr Kenneth Kaunda District Municipality', 8)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Dr Ruth Segomotsi Mompati District Municipality', 8)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Eden District Municipality', 9)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Ehlanzeni District Municipality', 6)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Ekurhuleni Metropolitan Municipality', 3)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('eThekwini Metropolitan Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Fezile Dabi District Municipality', 2)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Frances Baard District Municipality', 7)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Gert Sibande District Municipality', 6)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('iLembe District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Joe Gqabi District Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('John Taolo Gaetsewe District Municipality', 7)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Lejweleputswa District Municipality', 2)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Mangaung Metropolitan Municipality', 2)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Mopani District Municipality', 5)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Namakwa District Municipality', 7)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Nelson Mandela Bay Metropolitan Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Ngaka Modiri Molema District Municipality', 8)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Nkangala District Municipality', 6)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('OR Tambo District Municipality', 1)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Overberg District Municipality', 9)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Pixley ka Seme District Municipality', 7)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Sedibeng District Municipality', 3)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Sekhukhune District Municipality', 5)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Sisonke District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Siyanda District Municipality', 7)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Thabo Mofutsanyana District Municipality', 2)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Ugu District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('uMgungundlovu District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('uMkhanyakude District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('uMzinyathi District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('uThukela District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('uThungulu District Municipality', 4)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Vhembe District Municipality', 5)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Waterberg District Municipality', 5)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('West Coast District Municipality', 9)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('West Rand District Municipality;', 3)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Xhariep District Municipality', 2)");
            Sql("INSERT INTO Districts(DistrictName , ProvinceId) VALUES('Zululand District Municipality', 4)");
        }
        
        public override void Down()
        {
        }
    }
}
