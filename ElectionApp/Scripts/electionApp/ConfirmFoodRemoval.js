﻿function ConfirmFoodRemoval()
{
    $(".js-remove").click(function (e) {
        var removeFood = $(e.target);
        bootbox.dialog({
            title: 'Confirm',
            message: "Are you sure you want to remove this food?",
            buttons: {
                Cancel: {
                    label: "Cancel",
                    className: 'btn-default',
                    callback: function () {
                        bootbox.hideAll();
                    }
                },
                Remove: {
                    label: "Remove",
                    className: 'btn-danger',
                    callback: function () {
                        $.ajax({
                            url: "/api/favouritefoods/" + removeFood.attr("data-id"),
                            method: "DELETE"
                        })
                   .done(function () {
                       removeFood.parents("li").fadeOut(function () {
                           $(this).remove();
                       });
                   })
                   .fail(function () {
                       alert("Something failed!");
                   });
                    }
                }
            }
        });
    });
}