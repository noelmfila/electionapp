﻿function DateOfBirth() {
    $(".date-picker").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        yearRange: "-150:+0",
        dateFormat: 'yy/mm/dd'
    });
}