﻿using ElectionApp.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Http;

namespace ElectionApp.Controllers.Api
{
    [Authorize]
    public class FavouriteFoodsController : ApiController
    {
        private readonly ApplicationDbContext _context;

        public FavouriteFoodsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpDelete]
        public IHttpActionResult Remove(int id)
        {
            var userId = User.Identity.GetUserId();

            var removeFood = _context.FavouriteFoods
                .Single(f => f.Id == id &&
                             f.VoterUserId == userId);

            _context.FavouriteFoods.Remove(removeFood);
            _context.SaveChanges();
            return Ok();
        }
    }
}
