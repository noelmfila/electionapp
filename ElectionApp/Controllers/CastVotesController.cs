﻿using ElectionApp.Models;
using ElectionApp.ViewModels;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Mvc;
using ElectionApp.Persistance;


namespace ElectionApp.Controllers
{
    [Authorize]
    public class CastVotesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UnitOfWork _unitOfWork;          

        public CastVotesController()
        {
            _context = new ApplicationDbContext();
            _unitOfWork = new UnitOfWork(_context);
        }


        public ActionResult CastAVote(string query = null)
        {
            var regParties = _context.Parties.ToList();

            if (!string.IsNullOrWhiteSpace(query))
            { 
                regParties = regParties.Where(p =>
                    p.PartyName.Contains(query) ||
                    p.Acronym.Contains(query) ||
                    p.Representative.Contains(query)
                    ).ToList();
            }

            var viewModel = new CastVotesViewModel
            {
                Parties = regParties,
                SearchTerm = query
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CastVotesViewModel viewModel)
        {
            var userId = User.Identity.GetUserId();
            var partyId = viewModel.PartyId;

            if (_context.CastVotes.Any(v => v.VoterId == userId))
                return Content("You have already voted!");

            var castedVote = new CastVote
            {
                PartyId = partyId,
                VoterId = userId
            };

            _context.CastVotes.Add(castedVote);

            _unitOfWork.Complete(); 
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
    }
}