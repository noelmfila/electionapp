﻿using AutoMapper;
using ElectionApp.Models;
using ElectionApp.Persistance;
using ElectionApp.ViewModels;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace ElectionApp.Controllers
{
    [Authorize]
    public class VotersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UnitOfWork _unitOfWork;

        public VotersController()
        {
            _context = new ApplicationDbContext();
            _unitOfWork = new UnitOfWork(_context);
        }

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
           
            var voterInfo = _context.Voters
                              .Include(v => v.Race)
                              .Include(v => v.Province)
                              .Include(v => v.Municipality)
                              .Include(v => v.District)
                              .Single(v => v.UserId == userId);

            if (voterInfo == null)
                return HttpNotFound();

            return View(voterInfo);
        }

        [HttpGet]
        public ActionResult GetDistrictsByProvinceId(string provinceId)
        {
            var districts = _unitOfWork.Districts.GetDistrictsByProvinceId(provinceId);
            return Json(districts, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMunicipalitiesByDistrictId(string districtId)
        {
            var districts = _unitOfWork.Municipalities.GetMunicipalitiesByDistrictId(districtId);
            return Json(districts, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VoterInfo(Voter voter)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("CreateVoter", voter);

            if (string.IsNullOrWhiteSpace(voter.UserId))
            {
                voter.UserId = User.Identity.GetUserId();
                _context.Voters.Add(voter);
            }
            else
            {
                var savedVoter = _context.Voters.Single(v => v.UserId == voter.UserId);
                Mapper.Map(voter, savedVoter);
            }
       
            _unitOfWork.Complete();

            return RedirectToAction("Index");
        }

        public ActionResult CreateVoter()
        {
            var raceName = _context.Races.ToList();
            var province = _unitOfWork.Provinces.GetProvinces();
            var district = _unitOfWork.Districts.GetDistrictsByProvinceId();
            var municipality = _unitOfWork.Municipalities.GetMunicipalitiesByDistrictId();

            var viewModel = new CreateVoterViewModel
            {
                Heading = "Create Voter",
                Races = raceName,
                Provinces = province,
                Districts = district,
                Municipalities = municipality
            };

            return View(viewModel);
        }

        public ActionResult EditVoter(string Id)
        {
            var voter = _context.Voters.SingleOrDefault(v => v.UserId == Id);

            if (voter == null)
                return HttpNotFound();

            var raceName = _context.Races.ToList();
            var province = _unitOfWork.Provinces.GetProvinces();
            var district = _unitOfWork.Districts.GetDistrictsByProvinceId();
            var municipality = _unitOfWork.Municipalities.GetMunicipalitiesByDistrictId();

            var viewModel = new CreateVoterViewModel
            {
                Voter = voter,
                Heading = "Edit Voter",
                Races = raceName,
                Provinces = province,
                Districts = district,
                Municipalities = municipality
            };

            return View("CreateVoter", viewModel);
        }


        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

    }
}