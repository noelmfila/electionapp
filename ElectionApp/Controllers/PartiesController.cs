﻿using ElectionApp.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ElectionApp.Controllers
{
    [Authorize(Roles = RoleName.CanManageParties)]

    public class PartiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PartiesController()
        {
          _context = new ApplicationDbContext();
        }
            
        public ActionResult Index()
        {
            return View(_context.Parties.ToList());
        }

        // GET: Parties/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var party = _context.Parties.Find(id);
            if (party == null)
                return HttpNotFound();

            return View(party);
        }

        // GET: Parties/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Parties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PartyName,Acronym,Representative,RegistrationDate")] Party party)
        {
            if (!ModelState.IsValid)
                return View(party);

            _context.Parties.Add(party);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Parties/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            var party = _context.Parties.Find(id);

            if (party == null)
                return HttpNotFound();
            
            return View(party);
        }

        // POST: Parties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PartyName,Acronym,Representative,RegistrationDate")] Party party)
        {
            if (!ModelState.IsValid)
                return View(party);

            _context.Entry(party).State = EntityState.Modified;
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Parties/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var party = _context.Parties.Find(id);

            if (party == null)
                return HttpNotFound();
            
            return View(party);
        }

        // POST: Parties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var party = _context.Parties.Find(id);
            _context.Parties.Remove(party);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();

            base.Dispose(disposing);
        }
    }
}
