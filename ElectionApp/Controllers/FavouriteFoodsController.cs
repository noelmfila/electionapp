﻿using System.Collections.Generic;
using ElectionApp.Dtos;
using ElectionApp.Models;
using ElectionApp.ViewModels;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace ElectionApp.Controllers
{
    [Authorize]
    public class FavouriteFoodsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FavouriteFoodsController()
        {
            _context = new ApplicationDbContext();
        }


        public ActionResult Index()
        {
            return View(_context.FavouriteFoods
                                .Include(f => f.Food)
                                .ToList());
        }


        public ActionResult Create()
        {
           if (!RemainingFoodsList().Any())
            {
                return RedirectToAction("Index");
            }

            var viewModel = new FavouriteFoodsViewModel
            {
                Foods = RemainingFoodsList()
            };

            return View(viewModel);
        }


        [HttpPost]
        public ActionResult Create(FavouriteFoodsDto dto)
        {
            var userId = User.Identity.GetUserId();

            var favouriteFood = new FavouriteFood()
            {
                VoterUserId = userId,
                FoodId = dto.FoodId
            };

            _context.FavouriteFoods.Add(favouriteFood);
            _context.SaveChanges();

            return RedirectToAction("Create");

        }

        public ActionResult Edit(int id)
        {
            if (!RemainingFoodsList().Any())
            {
                return RedirectToAction("Index");
            }

            var viewModel = new FavouriteFoodsViewModel
            {
                FavouriteId = id,
                Foods = RemainingFoodsList()
            };

            return View("Edit", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FavouriteFoodsViewModel viewModel)
        { 
            var userId = User.Identity.GetUserId();

            var editFood = _context.FavouriteFoods
                                   .Single(f => f.Id == viewModel.FavouriteId &&
                                  f.VoterUserId == userId);

            editFood.VoterUserId = userId;
            editFood.FoodId = viewModel.SelectedFoodId;
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public List<Food> RemainingFoodsList()
        {
            return _context.Foods
                           .Where(food => !_context.FavouriteFoods
                           .Any(f => f.FoodId == food.Id))
                           .ToList();
        }
    }
}
