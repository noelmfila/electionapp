﻿using ElectionApp.Models;
using ElectionApp.Repository;
using System.Web.Mvc;

namespace ElectionApp.Controllers
{   [Authorize(Roles = RoleName.CanManageParties)]
    public class ElectionResultsController : Controller
    {
        private readonly ResultsRepository _results;

        public ElectionResultsController()
        {
            var context = new ApplicationDbContext();
            _results = new ResultsRepository(context);
        }

        public ActionResult Results()
        {
            return View(_results.GetResults());
        }
    }
}