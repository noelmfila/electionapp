﻿using ElectionApp.Models;
using ElectionApp.Repository;

namespace ElectionApp.Persistance
{
    public class UnitOfWork
    {
        private readonly ApplicationDbContext _context;

        public ProvincesRepository Provinces { get; }
        public DistrictsRepository Districts { get; }
        public MunicipalitiesRepository Municipalities { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Provinces = new ProvincesRepository(context);
            Districts = new DistrictsRepository(context);
            Municipalities = new MunicipalitiesRepository(context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }

    }
}